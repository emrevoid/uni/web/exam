# uni-web

## Stack usage

```bash
$ mkdir www # see https://github.com/fam4r/uni-web/issues/1 for reason
$ docker-compose up -d
```

## Misc

### jQuery include

```html
<script src="https://code.jquery.com/jquery-1.11.3.min.js" integrity="sha256-7LkWEzqTdpEfELxcZZlS6wAx5Ff13zZ83lYO2/ujj7g=" crossorigin="anonymous"></script>
```

## Capitoli focus

- codifiche
- accessibilità
  - wcag
  - wcag 2.0
  - wcag / stanca
  - possibili problemi sui seguenti elementi forniti
    - immagini
    - tabelle
    - form
- novità delle nuove versioni di alcune tecnologie rispetto alle precedenti
  - html5
  - CSS3
  - solution stack

## Domande teoria

- Descrivere brevemente le principali differenze e analogie tra i solution stack LAMP e MEAN

- Descrivere brevemente le principali novità introdotte da CSS3
  - `background-size`
  - `background-origin`
  - `background: url(...) opzioni, url(...), [...]`
  - `background: linear-gradient(direction, color-stop1, color-stop2, ...)`
  - `background: repeating-linear-gradient`
  - `background: radial-gradient`
  - `background: repeating-radial-gradient`
  - `border-image`
  - `border-radius`
  - regole specifiche browsers

- Dare una definizione di user experience e descrivere un caso di design centrato sull’utente
  - la **user experience** descrive la reazione dell'utente di fronte all'interazione con lo strumento in base a tre dimensioni:
    - **pragmatica**: funzionalità e usabilità del sistema
    - **estetica/edonistica**: piacevolezza estetica, emotiva e ludica del sistema
    - **dimensione simbolica**: attributi sociali, forza del brand, identificazione
  - Personas e Scenarios sono un esempio di design user-centered
    - Personas: descrizione degli utenti rappresentativi (proprietà specifiche, anche gruppi di utenti, dai 3 ai 7 a progetto)
    - Scenarios: sequenza di azioni che una persona compie utilizzando il servizio. Include, per ciascuna personas
      - obiettivi / goal
      - motivazioni
      - contesto: chi, dove, quali tecnologie
      - distrazioni e come le affrontano le personas

- Elencare e descrivere brevemente i livelli di accessibilità specificati nelle WCAG (x2):
  - Livello A: alternativa testuale; contenuti adattabili
  - Livello AA: contrasto minimo
  - Livello AAA

- Descrivere le principali differenze tra le WCAG 2.0 e i requisiti definiti nell’ambito della Legge Stanca (L. 4/2004) (x2)
  - il numero di requisiti è lo stesso
  -

- Elencare e descrivere brevemente i principi specificati nelle WCAG (x2)
  - percepibile:
    - Linea guida 1.4 Distinguibile: Rendere più semplice agli utenti la visione
      e l'ascolto dei contenuti, separando i contenuti in primo piano dallo sfondo
    - alternativa testuale
    - adattabile
  - utilizzabile
  - comprensibile
  - robusto

- Descrivere brevemente le metodologie di codifica dei caratteri nelle pagine Web:
  - ASCII: 7 bit (un byte ma un bit non utilizzato)
  - ISO Latin 1: 8 bit
    - primi 128 bit == ASCII
    - altri 128 caratteri latini specifici
  - ISO/IEC 10646: 4 byte
    - 17 piani
    - UTF-16: UCS-2: 2 byte
    - UTF-8: UCS-4: 4 byte

- Descrivere brevemente le metodologie di codifica dei colori nelle pagine Web e nei fogli di stile CSS (x2):
  - keyword (`red`, `green` ...)
  - esadecimale (`#RRGGBB`)
  - decimale: `rgb(r, g, b)` con r,g,b € {0, ..., 255}
  - decimale con trasparenza: `rgb(r, g, b, opa)`
  - HSL: `hsl(h, s, l)`
  - HSLA: `hsla(h, s, l, alpha)`

- Descrivere cosa si intende in ambito Web con usabilità e cosa con accessibilità, sottolineando le differenze (x2)

- Descrivere brevemente le principali differenze tra markup e metamarkup:
  - **markup**: linguaggio per annotare documento fornendo interpretazione delle sue parti
  - **metamarkup**: regole di interpretazione di un linguaggio markup (anche per la definizione di nuovi linguaggi)

- Descrivere brevemente le principali novità introdotte da HTML5
  - categorie di elementi
  - elementi semantici: `header`, `footer`, `section`, `article`

- Descrivere le principali differenze tra usability e user experience

- Descrivere le principali differenze tra progressive enhancement e graceful degradation

- Descrivere le principali differenze tra i metodi GET e POST

- Descrivere brevemente comporta il fatto che http sia stateless e come si supera
