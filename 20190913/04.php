<?php

$dbserver = 'db';
$database = 'settembre';
$username = 'root';
$password = 'toor';
$table = 'starwars';

// Create connection
$conn = new mysqli($dbserver, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Database connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // collect value of input field
    $name = $_POST['name'];
    $height = $_POST['height'];
    $mass = $_POST['mass'];

    if (empty($name) || empty($height) || empty($mass)) {
        echo "Some fields are empty";
    } else {
        // prepare and bind
        $stmt = $conn->prepare("INSERT INTO " . $table . " (name, height, mass) VALUES (?, ?, ?)");
        $stmt->bind_param("sii", $name, $height, $mass);

        $stmt->execute();

        echo "New records created successfully";

        $stmt->close();
    }
}
elseif ($_SERVER["REQUEST_METHOD"] === "GET") {
  $sql = "SELECT name, height, mass FROM ".$table;
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    $characters = array();

    while($row = $result->fetch_assoc()) {
      $name = $row["name"];
      $height = $row["height"];
      $mass = $row["mass"];

      $character = array("name" => $name, "height" => $height, "mass" => $mass);

      array_push($characters, $character);
    }

    $characters_json = json_encode($characters);
    print_r($characters_json);
  } else {
      echo "0 results";
  }
}

$conn->close();
?>
