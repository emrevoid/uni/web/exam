$(document).ready(function(){
  getCharacters();
  $("#btnAddCharacter").click(function() {
    name = $("#name").val();
    height = $("#height").val();
    mass = $("#mass").val();
    if(name.length !== 0 || height.length !== 0 || mass.length != 0) {
      $.post("04.php",
        {
            name: name,
            height: height,
            mass: mass
        }, function() {
          getCharacters();
        });
    }
  });
});

function getCharacters() {
  $.get("04.php", function(data, status){
    let table = document.createElement("table");
    let tableId = "characters";
    $(table).attr("id", tableId);

    $("body").remove("#" + tableId);
    $("body").append(table);

    let nameColumn = "Name";
    let heightColumn = "Height";
    let massColumn = "Mass";
    addCharacter(table, nameColumn, heightColumn, massColumn);

    let characters = JSON.parse(data);
    for(character of characters) {
      addCharacter(table,
                  character.name,
                  character.height,
                  character.mass);
    }
  });
}

function addCharacter(table, name, height, mass) {
  let row = table.insertRow(-1);

  nameCell = row.insertCell(0);
  heightCell = row.insertCell(1);
  massCell = row.insertCell(2);

  nameCell.innerHTML = name;
  heightCell.innerHTML = height;
  massCell.innerHTML = mass;
}
