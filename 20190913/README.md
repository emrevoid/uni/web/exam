# 20190913

## 01

- `label`'s `for` is related to element ID
- `label` can nest elements if you don't want to specify IDs
- `input` submit button doesn't need a `label` for accessibility, `value` is enough

## 03 (TEORIA)

Descrivere brevemente le principali differenze e analogie tra i solution stack LAMP e MEAN:

I solution stack LAMP e MEAN forniscono entrambi un ambiente applicativo di sviluppo web, in particolare rendono disponibili le tecnologie atte a servire le pagine web (in termini di web server e processing backend), nonché gestire e memorizzare i dati.

LAMP si costituisce di:
- Linux come sistema operativi
- Apache come web server
- MySQL come database
- PHP/Python/Perl come linguaggio di programmazione (backend)

MEAN fornisce un approccio differente, pur perseguendo l'idea originale di servire un solution stack per lo sviluppo web.
MEAN, infatti, utilizza:
- MongoDB come NoSQL driver per l'immagazinamento dei dati
- Express come framework di sviluppo lato server
- Angular come framework di sviluppo lato client
- Node come web server

Le principali differenze tra MEAN e LAMP sono quindi:
- MEAN è multipiattaforma; L(/W)AMP è legato ad un sistema operativo
- MEAN utilizza Node come web server che è ad alta concorrenza ed elevate prestazioni
- MEAN usa un DB non relazionale, a differenza di LAMP che usa un DB relazionale
- MEAN definisce due framework, uno client e uno server, mentre LAMP solamente un linguaggio di programmazione lato server

## 05

- use `$.getJSON` instead of `$.get` + `JSON.parse`
- directly append HTML elements instead of constructing them with proper functions
- use `length` property to check if form elements are set
