<?php

$soglia1 = $_POST['soglia1'];
$soglia2 = $_POST['soglia2'];

if(!isset($soglia1) || !isset($soglia2)) {
  trigger_error("Parametri soglia1 e soglia2 non impostati!");
}

if(!is_int($soglia1) || !is_int($soglia2)) {
  trigger_error("Parametri soglia1 e soglia2 non sono interi!");
}

if($soglia1 < 0 || $soglia2 < 0) {
   trigger_error("Parametri soglia1 e soglia2 sono negativi!");
}

$numeri = array();

for($i = $soglia1; $i < $soglia2; $i++) {
  array_push($numeri, $i);
}

$is_ordered = true;

do {
  $is_ordered = true;
  for($i = 0; $i < count($numeri) - 1; $i++) {
    if($numeri[$i] < $numeri[$i + 1]) {
      $is_ordered = false;
      $tmp = $numeri[$i];
      $numeri[$i] = $numeri[$i + 1];
      $numeri[$i + 1] = $tmp;
    }
  }
}while (!$is_ordered);

$numeri_json = json_encode($numeri);

echo $numeri_json;

?>
